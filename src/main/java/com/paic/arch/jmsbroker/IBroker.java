package com.paic.arch.jmsbroker;

/**
 * 定义Broker的接口
 * @author 王谦益
 *
 */
public interface IBroker {

	public IBroker bindToBrokerAt(String aBrokerUrl);
	
	public IBroker andThen();
	
	public IBroker sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
	
	public String retrieveASingleMessageFromTheDestination(String aDestinationName);
	
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);

	public String getBrokerUrl();
	
	public void stopTheRunningBroker() throws Exception;

	public long getEnqueuedMessageCountAt(String testQueue) throws Exception;
}
