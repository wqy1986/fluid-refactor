package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

/**
 * BrokerService是ActiveMq特有的，因此对BrokerService的操作都要抽取出来
 * @author 王谦益
 *
 */
public class ActiveMqBroker extends NewJmsMessageBrokerSupport implements IBroker {

	private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;
    private BrokerService brokerService;
    
	private ActiveMqBroker(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
        super.setBrokerUrl(aBrokerUrl);
    }

    @Override
    public final String getBrokerUrl() {
        return brokerUrl;
    }

	@Override
	public IBroker bindToBrokerAt(String aBrokerUrl) {
		return new ActiveMqBroker(aBrokerUrl);
	}

	@Override
	public IBroker sendATextMessageToDestinationAt(String aDestinationName,
			String aMessageToSend) {
		return super.sendATextMessageToDestinationAt(aDestinationName,
				aMessageToSend);
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(
			String aDestinationName) {
		return super.retrieveASingleMessageFromTheDestination(
				aDestinationName);
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(
			String aDestinationName, int aTimeout) {
		return super.retrieveASingleMessageFromTheDestination(
				aDestinationName, aTimeout);
	}

    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }


    private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    @Override
    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    public static ActiveMqBroker bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
    	return  new ActiveMqBroker(aBrokerUrl);
    }

    public static IBroker createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        ActiveMqBroker broker = bindToBrokerAtUrl(aBrokerUrl);
        broker.createEmbeddedBroker();
        broker.startEmbeddedBroker();
        return broker;
    }

    public static IBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }
}
